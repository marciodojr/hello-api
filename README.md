## Instruções

### Comandos iniciais

Execute os comandos:

```sh
## first terminal
cp .env.example .env
## devido a falta de dependencias do composer é normal nesse primeiro momento o container helloapp-queue dar problema
docker-compose up
## second terminal
docker exec -it helloapp-app bash
## install composer dependencies
composer install
## run migrations (fresh database)
php artisan migrate:fresh
## third terminal
mysql -u root -p helloapp --port 13306 -h 127.0.0.1
## password: helloapp
show tables
```

Reinicie os contêineres (Ctrl + C no terminal que está rodando `docker-compose up`)

Obs: os logs são salvos em storage/logs. É bastante útil deixar o log corrente aberto (via tail -f <logfilename>.log durante os testes)

### Fila de processos

A fila de processos fica ativa indefinidamente via *service* *queue* (ver `docker-compose.yaml`)

### listar comandos do artisan

```sh
docker exec -it helloapp-app bash
php artisan list
```


### Salvar um nome

```
curl -d "firstname=Marcio" -X POST http://localhost:8080/hello | jq
```

### Buscar um nome

```
curl -X GET http://localhost:8080/hello | jq
```