<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => ''], function () use ($router) {
    // root
    $router->get('', function () use ($router) {
        return response([
            'code' => 200,
            'message' => 'ok',
        ], 200);
    });
    $router->group(['prefix' => '/hello'], function () use ($router) {
        $router->get('', 'HelloController@index');
        $router->post('', 'HelloController@store');
        $router->get('/{id}', 'HelloController@show');
    });
});
