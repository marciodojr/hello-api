<?php

use Faker\Generator as Faker;

$factory->define(App\Entities\Person::class, function (Faker $faker) {
    return [
        'firstname' => $faker->firstName()
    ];
});
