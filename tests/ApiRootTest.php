<?php

namespace AppTests;

use Laravel\Lumen\Testing\DatabaseMigrations;


class ApiRootTest extends TestCase
{
    /**
     * Check "/" endpoint
     *
     * @return void
     */
    public function testSuccess()
    {
        $this->get('/');

        $this->assertResponseStatus(200);
        $this->assertEquals([
            'code' => 200,
            'message' => 'ok'
        ], $this->response->getOriginalContent());
    }
}
