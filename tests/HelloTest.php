<?php

namespace AppTests;

use App\Entities\Person;

class HelloTest extends TestCase
{
    public function testSavePerson()
    {
        $person = factory(Person::class)->make();

        $postData = [
            'firstname' => $person->firstname
        ];

        $this->post('/hello', $postData);

        $this->assertResponseStatus(201);
        $data = $this->response->getOriginalContent();

        $this->assertArrayHasKey('code', $data);
        $this->assertArrayHasKey('message', $data);
        $this->assertArrayHasKey('data', $data);

        $this->seeInDatabase('people', $postData);
    }

    public function testListDefault()
    {
        factory(Person::class, 100)->create();

        $this->get('/hello');

        $this->assertResponseStatus(200);
        $data = $this->response->getOriginalContent();

        $this->assertArrayHasKey('code', $data);
        $this->assertArrayHasKey('message', $data);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('items', $data['data']);
        $this->assertCount(10, $data['data']['items']);
    }

    public function testList25()
    {
        factory(Person::class, 100)->create();

        $this->get('/hello?amount=25');

        $this->assertResponseStatus(200);
        $data = $this->response->getOriginalContent();

        $this->assertArrayHasKey('code', $data);
        $this->assertArrayHasKey('message', $data);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('items', $data['data']);
        $this->assertCount(25, $data['data']['items']);
    }

    public function testListMax100()
    {
        factory(Person::class, 150)->create();

        $this->get('/hello?amount=120');

        $this->assertResponseStatus(200);
        $data = $this->response->getOriginalContent();

        $this->assertArrayHasKey('code', $data);
        $this->assertArrayHasKey('message', $data);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('items', $data['data']);
        $this->assertCount(100, $data['data']['items']);
    }

    public function testListSpecificPerson()
    {
        $person = factory(Person::class)->create();

        $this->get("/hello/$person->id");

        $data = $this->response->getOriginalContent();

        $this->assertArrayHasKey('code', $data);
        $this->assertArrayHasKey('message', $data);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('id', $data['data']);
        $this->assertEquals($person->id, $data['data']['id']);
        $this->assertArrayHasKey('firstname', $data['data']);
    }

    public function testDoNotListSpecificPerson()
    {
        factory(Person::class)->create();

        $this->get("/hello/-10");

        $data = $this->response->getOriginalContent();

        $this->assertArrayHasKey('code', $data);
        $this->assertArrayHasKey('message', $data);
        $this->assertArrayHasKey('data', $data);
        $this->assertEmpty($data['data']);
    }
}
