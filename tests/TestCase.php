<?php

namespace AppTests;

use Laravel\Lumen\Testing\TestCase as BaseTestCase;
use Laravel\Lumen\Testing\DatabaseTransactions;


abstract class TestCase extends BaseTestCase
{
    use DatabaseTransactions;
    use JwtGenerator;
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }
}
