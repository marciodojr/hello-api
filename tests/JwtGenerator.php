<?php

namespace AppTests;

use App\Entities\User;
use App\Services\JwtWrapper;

trait JwtGenerator
{
    protected function generateJWTTokenHeader(bool $isAdmin)
    {
        $user = factory(User::class)->create([
            'is_admin' => $isAdmin
        ]);

        $jwt = $this->app->make(JwtWrapper::class);

        return $jwt->encode($user);
    }

    protected function generateBearerHeader(string $token)
    {
        return [
            'Authorization' => 'Bearer ' . $token
        ];
    }

    protected function decodeJWTokenHeader(string $token)
    {
        return $this->app->make(JwtWrapper::class)->decode($token);
    }
}