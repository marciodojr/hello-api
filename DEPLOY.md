## GCloud

### Definições iniciais de configuração

```
gcloud config set compute/zone us-east1-b
gcloud config set project mdojr-kubelearning
```

### Criando IP e certificados

```sh
gcloud compute addresses create hello-ingress-ip --global
# associe o ip gerado, aos nomes abaixo em seu dns hosting (registro.br, cloud dns, ...)
gcloud beta compute ssl-certificates create gdgk8s-hello-api --domains "hello-api.mdojr.com.br"
gcloud beta compute ssl-certificates create gdgk8s-hello --domains "hello.mdojr.com.br"
gcloud beta compute ssl-certificates list
```

### Criando um cluster inicial

```
gcloud container clusters create gdgk8s \
    --machine-type=n1-standard-1 \
    --num-nodes=1 \
    --min-nodes=1 \
    --max-nodes=3 \
    --enable-autoscaling \
    --tags=standard
```

### Criando um pool adicional

```
gcloud container node-pools create small \
    --machine-type=g1-small \
    --num-nodes=1 \
    --min-nodes=1 \
    --max-nodes=3 \
    --enable-autoscaling \
    --tags=small \
    --cluster=gdgk8s
```

### Aumentando o tamanho do cluster

```
gcloud container clusters resize gdgk8s \
    --size=3
    --node-pool=small
```

### Verificando o cluster

```
gcloud container clusters list
```

### Utilizando o cluster com o kubectl

```sh
gcloud container clusters get-credentials gdgk8s
kubectl get nodes
kubectl get services
```

### Kubernetes

Obs: caso queira conectar com o banco de dados sem proxy crie o banco de dados com ip publico e adicione acesso geral CIDR: 0.0.0.0/0

Secret para o Cloud Sql (Seguir os passos 1 e 2 de https://cloud.google.com/sql/docs/mysql/connect-container-engine?hl=pt-br).
Salvar o arquivo em **kubernetes/credentials com** o nome **cloud-sql-proxy-credentials.json**

### Secrets

```
kubectl create secret generic cloudsql-credentials \
    --from-file=credentials.json=kubernetes/credentials/cloud-sql-proxy-credentials.json
kubectl apply -f kubernetes/secrets.yaml
```

### PersistentVolumes

```
kubectl apply -f kubernetes/persistent-volume-claim.yaml
```

### Deployments & Services

```sh
kubectl apply -f kubernetes/api.yaml
kubectl apply -f kubernetes/app.yaml
kubectl apply -f kubernetes/queue.yaml
```

### Autoscaling

```
kubectl autoscale deployment hello-api --min=1 --max=5 --cpu-percent=80
kubectl autoscale deployment hello-app --min=2 --max=5 --cpu-percent=80
kubectl autoscale deployment hello-queue --min=2 --max=5 --cpu-percent=80
kubectl get hpa
```

### Ingress

Criando o Ingress
```
kubectl apply -f kubernetes/ingress.yaml
```

### Descrições, Logs e acesso aos pods

```
kubectl describe pod [pod-name]
kubectl logs [pod-name] [container-name]
kubectl exec -it [pod-name] -c [container-name] bash
```

### Acessando a aplicação

- https://helm-example.mdojr.com.br