### Gerar imagem da aplicação

```
docker build -f docker/app/Dockerfile -t hello-api .
```

### Gerar imagem da *queue*

```
docker build -f docker/queue/Dockerfile -t hello-queue .
```

### Local gitlab runner

```
gitlab-runner exec docker test
gitlab-runner exec docker docker-build \
    --docker-privileged \
    --env="CI_REGISTRY_PASSWORD=password" \
```

Gitlab Runner

Adicione a variável CI_REGISTRY_PASSWORD nas variáveis de ambiente do projeto