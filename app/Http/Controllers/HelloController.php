<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Person;

class HelloController extends Controller
{
    private $person;

    public function __construct(Person $p)
    {
        $this->person = $p;
    }

    public function index(Request $request)
    {
        $data = $this->validate($request, [
            'amount' => 'integer|min:1',
            'page' => 'integer|min:1'
        ]);

        $amount = min($data['amount'] ?? 10, 100);
        $pageSize = $data['page'] ?? 1;

        $people = $this->person
            ->orderBy('id', 'desc')
            ->limit($amount)
            ->offset(($pageSize - 1) * $amount)
            ->get();

        $total = $this->person->count();

        return $this->response([
            'items' => $people->toArray(),
            'total' => $total
        ], 200);
    }

    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'firstname' => 'required|min:2|max:255',
        ]);

        $person = $this->person->create($data);

        return $this->response($person->toArray(), 201);
    }

    public function show(Request $request, int $id)
    {
        $person = $this->person->find($id);
        return $this->response($person ? $person->toArray() : [], 200);
    }
}
