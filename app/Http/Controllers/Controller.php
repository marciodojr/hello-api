<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class Controller extends BaseController
{
    protected function response(array $data = [], int $code = 200, $message = 'ok') : JsonResponse
    {
        return response()->json([
            'code' => $code,
            'data' => $data,
            'message' => $message
        ], $code);
    }
}
