<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Log\Logger;
use Illuminate\Http\Request;

class RequestLogMiddleware
{
    private $logger;

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);

        $this->logger->debug(sprintf(
            "\n%s /%s:\n%s",
            $request->method(),
            $request->path(),
            \json_encode($request->all(), JSON_PRETTY_PRINT ^ JSON_UNESCAPED_SLASHES)
        ));

        return $response;
    }
}
